using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.Sensor;

//
// cadence/speed/distance based loosly on my Hyundai dash :)
// Nick Macias July 2020
// Released under Creative Commons Attribution International 4.0
// Feel free to use/modify/re-distribute/sell/whatever, provided
// you retain this attribution and license
//
// No warranty here! This should work, but use it only in non-critical situations.
//

class RetroGaugesView extends WatchUi.DataField {
// made for Forerunner 920XT, may also work on VivoActive
// (only made for square displays similar in size to the Forerunner)

  var cadence=0;
  var speed=0;
  var distance=0; // update these in the compute() method
  var tdistance; // trip distance
  var dialLabel; // evil global (too many args?!?)
  var history; // are we supposed to save/re-use history?
  var mileage=0; // for saving in properties
  var lastInt; // last integer mileage
  var rolling=false; // used to detect when we stop
  var heading=0; // radians from north
  var doHeading=false; // should we show compass direction?
  var font=Graphics.FONT_SYSTEM_LARGE; // for dial digits
  var showTDistance=true;

  function initialize() { // kinda boring...
    DataField.initialize();
    history = Application.getApp().getProperty("history");
    if (history){
      mileage = Application.getApp().getProperty("mileage"); // add this to current distance
    }
    distance=mileage;
    tdistance=0; // trip distance
    showTDistance = Application.getApp().getProperty("tdistance");
    doHeading = Application.getApp().getProperty("compass");
  }

  function compute(info) { // called when there is new data available
    if (info == null){
      return;
    } // something weird...
    
// read cadence
    if (info.currentCadence != null){
      cadence=info.currentCadence;
      if (cadence > 125){
        cadence=125; // cap it here
      }
    }
    cadence=cadence.toFloat(); // so we can divide etc.

// read speed and distance
    if (info.currentSpeed != null){
      speed=info.currentSpeed*2.23694; // from meters per sec to mph
      if (speed > 35){
        speed=35;
      }
    }
    if (history && (speed==0) && rolling){ // we're stopped, but we were moving last check
      Application.getApp().setProperty("mileage",distance); // add this to current distance
      //System.println("Saving");
      rolling=false;
    }
    if (speed > 0){rolling=true;}

    if (info.elapsedDistance != null){
      tdistance=info.elapsedDistance*.06214; // from meters to hundredths of a mile
      distance=tdistance+mileage;
      var thisInt=(distance/100).toLong();
      if (history && (thisInt != lastInt)){ // save mileage every mile (don't blow up flash?!?)
        //System.println("Saving");
        Application.getApp().setProperty("mileage",distance); // add this to current distance
        lastInt=thisInt;
      }
    }
    distance=distance.toLong();
    tdistance=tdistance.toLong();
    //System.println("c=" + cadence + " s=" + speed + " d=" + distance);
    
// heading
    if (info.currentHeading != null){
      heading=info.currentHeading*57.296; // heading in degrees
      if (heading < 0){heading=heading+360;} // stick with positive angles
    }
  }

    // Display the value you computed here. This will be called
    // once a second when the data field is visible.
  function onUpdate(dc) {
    var width=dc.getWidth();
    var height=dc.getHeight();
    //var backgroundColor=getBackgroundColor();

// heading
    if (doHeading){
// better compass code
      drawHeading(dc,width,heading);
    }
    
// odometer
    font=Graphics.FONT_SYSTEM_LARGE;
    dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
// find digits
    var d1000=(distance/100000)%10; // thousands
    var d100=(distance/10000)%10; // hundreds
    var d10=(distance/1000)%10; // tens
    var d1=(distance/100)%10; // units
    var d0=(distance/10)%10; // tenths
    var prevScroll=distance%10; // how far to scroll tenths

// save amt of scroll from digit to right, so we don't phantom-scroll
    drawNum(dc,width/2 + 30,-9+height*4/5,19,34,d0,prevScroll,true);  // tenths (true flag means recervse video + clear all outside regions)
    drawNum(dc,width/2 + 10,-9+height*4/5,19,34,d1,(d0==9)?adjust(prevScroll,1):0,false); // units
    drawNum(dc,width/2 - 10,-9+height*4/5,19,34,d10,((d0==9)&&(d1==9))?adjust(prevScroll,1):0,false); // tens
    drawNum(dc,width/2 - 30,-9+height*4/5,19,34,d100,((d0==9)&&(d1==9)&&(d10==9))?adjust(prevScroll,2):0,false); // hundreds
    drawNum(dc,width/2 - 50,-9+height*4/5,19,34,d1000,((d0==9)&&(d1==9)&&(d10==9)&&(d100==9))?adjust(prevScroll,2):0,false); // thousands

// trip distance
    if (showTDistance){
      font=Graphics.FONT_SYSTEM_MEDIUM;
      d100=(tdistance/10000)%10; // hundreds
      d10=(tdistance/1000)%10; // tens
      d1=(tdistance/100)%10; // units
      d0=(tdistance/10)%10; // tenths
      prevScroll=tdistance%10; // how far to scroll tenths
      drawNum(dc,width/2 + 22,-27+height*1/5,15,28,d0,prevScroll,true);  // tenths (true flag means recervse video + clear all outside regions)
      drawNum(dc,width/2 +  6,-27+height*1/5,15,28,d1,(d0==9)?adjust(prevScroll,1):0,false); // units
      drawNum(dc,width/2 - 10,-27+height*1/5,15,28,d10,((d0==9)&&(d1==9))?adjust(prevScroll,1):0,false); // tens
      drawNum(dc,width/2 - 26,-27+height*1/5,15,28,d100,((d0==9)&&(d1==9)&&(d10==9))?adjust(prevScroll,2):0,false); // hundreds
    }

// tachometer and speed
    dialLabel="RPM x10"; // problem passing label ("too many arguments" error)
    drawMeter(dc,cadence.toFloat()/10,width/4,60+height/4,height/3,13,1,3,9);
    dialLabel="MPH";
    drawMeter(dc,speed,3*width/4,60+height/4,height/3,7,5,2,5);

/***
      var hstr="";
      if ((heading < 22.5) || (heading > 337.5)){
        hstr="N";
      } else if (heading < 67.5){
        hstr="NE";
      } else if (heading < 112.5){
        hstr="E";
      } else if (heading < 157.5){
        hstr="SE";
      } else if (heading < 202.5){
        hstr="S";
      } else if (heading < 247.5){
        hstr="SW";
      } else if (heading < 292.5){
        hstr="W";
      } else { // if (heading < 337.5)
        hstr="NW";
      }

      dc.setColor(Graphics.COLOR_RED,Graphics.COLOR_TRANSPARENT);
      //dc.drawText(20,height*4/5,Graphics.FONT_SYSTEM_LARGE,hstr,Graphics.TEXT_JUSTIFY_CENTER);
      dc.drawText(12,130,Graphics.FONT_SYSTEM_SMALL,hstr,Graphics.TEXT_JUSTIFY_CENTER);
***/
  }
  
 // simple mechanical hack
   function adjust(val,chg)
   {
     val=val-chg;
     if (Math.rand()%5==0) {val--;} // random mechanical lag
     return ((val>0)?val:0);
   }
    
// draw one digit of odometer
// (x1,y1) are coordinates of lower left corder
// w, h are width and height of rectangle
// val is digit to draw (two digits if last digit)
// dfrac is how much this digit should be scrolled (0 means no scroll)
// reverse is TRUE for last digit (reverse video)
  function drawNum(dc,x1,y1,w,h,val,dfrac,reverse)
  {
    dc.setPenWidth(1);

// draw background rectangle first
    dc.setColor(reverse?Graphics.COLOR_WHITE:Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT); // normally black
    dc.fillRectangle(x1,y1,w,h);

// and set foreground color
    dc.setColor(reverse?Graphics.COLOR_BLACK:Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT); // normally white

    var xoff;
    if (font==Graphics.FONT_SYSTEM_MEDIUM){
      xoff=7;
    } else {
      xoff=10;
    }
    
// if all digits to the right are scrolling, sdcroll this digit
    if (dfrac==0){ // normal
      dc.drawText(x1+xoff,y1+4,font,""+val,Graphics.TEXT_JUSTIFY_CENTER);
    } else { // scroll it!
// draw current digit, offset by scroll amount (dfrac)
      var newy=y1-(h.toFloat()*(dfrac.toFloat()/10));
      dc.drawText(x1+xoff,newy+4,font,""+val,Graphics.TEXT_JUSTIFY_CENTER); // current digit rolls off the top
// draw next digit
      newy=newy+h;
      dc.drawText(x1+xoff,newy+4,font,""+((val+1)%10),Graphics.TEXT_JUSTIFY_CENTER); // next digit comes from the bottom

// wipe out the excess
      dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT); // this is only happending on tenths (reverse=TRUE)
      dc.fillRectangle(x1,y1-h,w,h); // overwrite above the box
      dc.fillRectangle(x1,y1+h,w,h); // and below
    } // done with special handling

// now draw outline of the box
    dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
    dc.drawRectangle(x1,y1,w,h);
    return(dfrac); // which was possibly changed in the argument list
  }
    
// draw a meter
// value is the value for the needle
// (xc,yc) is the center of the dial
// rad is the radius of the dial
// numTics is the total number of tic marks to display
// valPerTic is how much each tic represents
// ticPerLabel says how often the tics are labeled
// criticalTic is for non-MPH dial, and says when to color-code RED
  function drawMeter(dc,value,xc,yc,rad,numTics,valPerTic,ticPerLabel,criticalTic)
  {
// outer arc
    dc.setPenWidth(3);
    if (dialLabel.equals("MPH")){
// 3 color ranges
      dc.setColor(Graphics.COLOR_GREEN,Graphics.COLOR_TRANSPARENT);
      dc.drawArc(xc,yc,rad,Graphics.ARC_CLOCKWISE,135,105);

      dc.setColor(Graphics.COLOR_ORANGE,Graphics.COLOR_TRANSPARENT);
      dc.drawArc(xc,yc,rad,Graphics.ARC_CLOCKWISE,105,75);
    
      dc.setColor(Graphics.COLOR_RED,Graphics.COLOR_TRANSPARENT);
      dc.drawArc(xc,yc,rad,Graphics.ARC_CLOCKWISE,75,45);
    } else { // rpm
      dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
      dc.drawArc(xc,yc,rad,Graphics.ARC_CLOCKWISE,135,45);
    
// high-value arc for RPM
      dc.setColor(Graphics.COLOR_RED,Graphics.COLOR_TRANSPARENT);
      dc.drawArc(xc,yc,rad,Graphics.ARC_CLOCKWISE,68,45);
    }

// draw tics
    dc.setPenWidth(1);
    dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
    var angle=-2.35; // first tics (angles are negative since Y runs from 0 at top to positive at bottom)

    var label;
    var len;
    
    for (var tic=0;tic<numTics;tic++){
      if ((tic%ticPerLabel) == 0){ // add a label
        label="" + (tic*valPerTic);
        len=13; // longer line
      } else {
        label="";
        len=6; // short line
// 11 and 8
      }
      
      if (dialLabel.equals("MPH")){ // set color bands differently here
        if (tic==0){
          dc.setColor(Graphics.COLOR_GREEN,Graphics.COLOR_TRANSPARENT);
        } // persists until next change :)
        if (tic==2){
          dc.setColor(Graphics.COLOR_ORANGE,Graphics.COLOR_TRANSPARENT);
        }
        if (tic==4){
          dc.setColor(Graphics.COLOR_RED,Graphics.COLOR_TRANSPARENT);
        }
      }
        
      if (tic==criticalTic){ // last few are in red!
        dc.setColor(Graphics.COLOR_RED,Graphics.COLOR_TRANSPARENT);
      }

// now draw the tick mark at the chosen length and with the chosen label
      tic(dc,xc,yc,rad,angle,len,label);
      angle+=1.5708/(numTics-1); // radians in one tic-to-tic arc, = (pi/2)/(# of tics - 1)
    } // end of tic drawing loop

// draw the gauge's label
    dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
//    dc.drawText(xc-7,yc+5,Graphics.FONT_TINY,dialLabel,Graphics.TEXT_JUSTIFY_CENTER);
    dc.drawText(xc-7,yc-7,Graphics.FONT_TINY,dialLabel,Graphics.TEXT_JUSTIFY_CENTER);
      
// draw needle
    dc.setPenWidth(3);
    angle=135-(value/(valPerTic*(numTics-1)))*90; // 0=125 deg, 120=45 deg
    angle=-angle*0.017453293; // convert to radians and flip for Y
    //dc.drawLine(xc,yc,xc+rad*Math.cos(angle),yc+rad*Math.sin(angle));
    dc.drawLine(xc+7*Math.cos(angle),yc+7*Math.sin(angle),xc+rad*Math.cos(angle),yc+rad*Math.sin(angle));
  }

// draw a single tic mark
// (xc,yc) is the center of the dial
// r is the radius
// angle is the angle :)
// len is how long the tic should be (towards the center)
// label is the label for the tic (or "" if no label)
  function tic(dc,xc,yc,r,angle,len,label)
  {
    var c=Math.cos(angle);
    var s=Math.sin(angle);
    var x1=xc+r*c;
    var y1=yc+r*s;
    var x2=xc+(r-len)*c;
    var y2=yc+(r-len)*s;
    dc.drawLine(x1,y1,x2,y2);
    if (!label.equals("")){
      dc.drawText(x1,y1-19,Graphics.FONT_XTINY,label,Graphics.TEXT_JUSTIFY_CENTER);
    }
  }

  function drawHeading(dc,width,h)
  {
    var dx=14;
    dc.setPenWidth(1);
    var labels=["N","NE","E","SE","S","sw","W","nw"];
    h=h+315-7;//-45; // go back 45 degrees
    var offset=h/(22.5/dx);offset=offset.toLong() % dx;offset=(dx-1)-offset;
    var ystart=4;
    var xstart=-3; // x loc
    var x=xstart+offset; // draw here
    var ind=(h/22.5)+65;ind=ind.toLong() % 16; // index from 0 to 15

    dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
    dc.fillRoundedRectangle(xstart+8,ystart,48,21,3);

// draw compass dial
    for (var i=0;i<5;i++){
      //System.println(i + " " + heading+" "+offset + " " + ind + " " + labels[ind.toNumber()/2]);
      if (ind%2==1){ // draw a line
        dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT);
        dc.drawLine(x,ystart+4,x,ystart+17);
      } else { // draw a label
        if (ind==0) {dc.setColor(Graphics.COLOR_RED,Graphics.COLOR_TRANSPARENT);} // NORTH!
        else {dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT);} 

        if ((ind==2)||(ind==6)||(ind==10)||(ind==14)) {dc.drawText(x,ystart+3,Graphics.FONT_SYSTEM_SMALL,labels[ind.toNumber()/2],Graphics.TEXT_JUSTIFY_CENTER);}
        else {dc.drawText(x,ystart,Graphics.FONT_SYSTEM_MEDIUM,labels[ind.toNumber()/2],Graphics.TEXT_JUSTIFY_CENTER);}
      }
      ind=(ind+1)%16; // new label
      x=x+dx; // new loc
    }
// draw marker
    //dc.drawRectangle(start,127,start+32,18);
    dc.setColor(Graphics.COLOR_ORANGE,Graphics.COLOR_TRANSPARENT);
    dc.drawLine(xstart+32,ystart,xstart+32,ystart+20);
// trim edges
    dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT);
    dc.fillRectangle(xstart-6,ystart,18,21);
    dc.fillRectangle(xstart+55,ystart,24,21);
  }
}